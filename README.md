# API Driven Blog

This repository hosts a simply blog that can be interacted with via a REST API.
It accepts either HTML or Org-mode source, which is automatically converted to HTML.
A detailed tutorial on the creation of this code is available at https:/gorrog.org/blog

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.
See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Python 3.7
Django
Docker
git

### Installing

In a directory of your choosing, paste this, followed by `ENTER` into a terminal.
```
git clone git@github.com:gorrog/mysite.git
```

Change into the new directory.
```
cd mysite
```

More details coming soon

## Running the tests

Coming soon

## Deployment

Coming soon

## Built With

,* [Python](https://www.python.org) - The Python programming language, version 3.7.3
,* [Django](https://www.djangoproject.com) - The web framework used.
,* [Django Rest Framework](https://www.django-rest-framework.org) - Adds a REST API to Django
,* [Docker](https://www.docker.com) - Used to containerise our project for easy deployment
,* [Pandoc](https://pandoc.org) - A universal converter tool between markup formats. Used to convert Org-mode source to HTML.
,* [pypandoc](https://pypi.org/project/pypandoc) - A Python wrapper around Pandoc to make it easy to interact with Pandoc via Python.

## Contributing

Should you wish to contribute to this project, feel free to open a pull request or issue.

## Authors

,* **Roger Gordon**

## License

This project is licensed under the MIT License.
